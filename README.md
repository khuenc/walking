# walking

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Report
- time spent on the project: 3h
- improve on if had more time:
+ add functions to the game (mini map, game initiation screen, login or trial function,
+ Structures code according to files with specific functions (api service, store, getter, setter)
+ Add effects to the game, like loading transitions,
